﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI1 : MonoBehaviour {

    public LayerMask movementMask;
    private Rigidbody rb;

    public float maxDistanceFromPoint = 5f;

    public float movementSpeed = 15f;  //variable that detirmines the force of movement forward
    public Vector3 moveDirection;



    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        moveDirection = chooseDirection();
        transform.rotation = Quaternion.LookRotation(moveDirection);
    }
	
	// Update is called once per frame
	void Update ()
    {
        rb.velocity = moveDirection * movementSpeed*Time.deltaTime;
        
        if (Physics.Raycast(transform.position, transform.forward, maxDistanceFromPoint, movementMask))
        {
            moveDirection = chooseDirection();
            transform.rotation = Quaternion.LookRotation(moveDirection);
        }
    }

    Vector3 chooseDirection()
    {
        System.Random ran = new System.Random();
        int i = ran.Next(0, 3);
        Vector3 temp = new Vector3();

        if (i==0)
        {
            temp = -transform.forward;
        }
        else if (i == 1)
        {
            temp = transform.forward;
        }
        else if (i == 2)
        {
            temp = transform.right;
        }
        else if (i == 3)
        {
            temp = -transform.right;
        }

        return temp;
    }
}
