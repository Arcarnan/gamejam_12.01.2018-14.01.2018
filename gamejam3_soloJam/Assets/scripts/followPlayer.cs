﻿using UnityEngine;

public class followPlayer : MonoBehaviour {

    public Transform player;
    public Vector3 offset;
    public float pitch = 2f;

    public float zoomSpeed = 4f;
    public float minZoom = 1f;
    public float maxZoom = 4f;
    private float currentZoom = 1f;

    public float yawSpeed = 100f;
    private float currentYaw = 0f;
    public float distance = 1f;

    private void Update()
    {
        currentZoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;  //zoom view of camera
        currentZoom = Mathf.Clamp(currentZoom, minZoom, maxZoom);

        currentYaw += Input.GetAxis("Horizontal") * yawSpeed * Time.deltaTime;  //"a" and "d" to move camera left and right
    }

    // Update is called once per frame
    void LateUpdate ()
    {
        transform.position = player.position + offset*currentZoom;  //offset for third person view
        transform.LookAt(player.position + Vector3.up*pitch);

        transform.RotateAround(player.position, Vector3.up, currentYaw);
        //transform.position = transform.position + Camera.main.transform.forward * distance * Time.deltaTime;
	}
}
