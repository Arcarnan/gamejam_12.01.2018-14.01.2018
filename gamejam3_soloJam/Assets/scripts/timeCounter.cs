﻿using UnityEngine;
using UnityEngine.UI;

public class timeCounter : MonoBehaviour {

    public playerCollision playerCylinder;

    public Transform player;
    public Text timer;
    public Text coordinates;
    public Text pickupScore;

    // Update is called once per frame
    void Update ()
    {
        coordinates.text = player.position.x.ToString("0") + ", " + player.position.z.ToString("0");
        timer.text = Time.timeSinceLevelLoad.ToString("0.00");
        pickupScore.text = "Mushrooms: " + playerCylinder.GetScore().ToString();
	}
}
