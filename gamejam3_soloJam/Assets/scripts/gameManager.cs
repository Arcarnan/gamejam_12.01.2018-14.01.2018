﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    bool gameEnded = false;

    public float restartDelay = 3f;
    public GameObject completeLevelUI;

    public void CompleteLevel()
    {
        completeLevelUI.SetActive(true);
    }

    public void endGame()
    {

        if (gameEnded==false)
        {
            gameEnded = true;
            Debug.Log("Game over. ");
            Invoke("Restart", restartDelay);
            //TODO display result
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //TODO when finished: go to window -> ligthing -> bottom, turn of auto (then press "generate lighting").
    }
}

