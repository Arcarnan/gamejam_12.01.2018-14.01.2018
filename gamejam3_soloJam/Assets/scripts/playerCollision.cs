﻿using UnityEngine;

public class playerCollision : MonoBehaviour
{
    public playerMovement movement;
    private int pickupCount;

    public GameObject[] mushrooms;

    private void Start()
    {
        pickupCount = 0;
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Enemy")
        {
            movement.enabled = false;  //stops movement if hit by an enemy
            //Destroy(movement.gameObject);
            FindObjectOfType<gameManager>().endGame();
        }

        if (collisionInfo.gameObject.CompareTag("Pickup"))
        {
            collisionInfo.gameObject.SetActive(false);
           // for (int i = 0; i < 5; i++)
           // {
           // Destroy(mushrooms[i]);
           // }
           pickupCount++;
        }
    }

    public int GetScore ()
    {
        return pickupCount;
    }
}
