﻿using UnityEngine;
using System.Collections;

public class EnemyAI2 : MonoBehaviour
{
    public float lookRadius = 10f;

    Transform player;
    public float rotationSpeed = 5.0f;
    public float movementSpeed = 8.0f;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        /* Look at Player*/
        transform.rotation = Quaternion.Slerp(transform.rotation
                                              , Quaternion.LookRotation(player.position - transform.position)
                                              , rotationSpeed * Time.deltaTime);

        /* Move towards Player*/
        transform.position += transform.forward * movementSpeed * Time.deltaTime;
    }

    //for seeing the look radius of the enemy objects
    private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, lookRadius);
        }
}

