﻿using UnityEngine;

public class playerMovement : MonoBehaviour
{

    public Rigidbody rb;

    public float movementSpeed = 10f;  //variable that detirmines the force of movement forward
    public float jumpForce = 1500f;        //variable that detirmines the force of a jump
    public float fallMultiplier = 20f;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))  //move forwards
        {
            transform.position += Camera.main.transform.forward * movementSpeed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))  //move backwards
        {
            transform.position -= Camera.main.transform.forward * movementSpeed * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (rb.position.y < 2)
            {

                rb.velocity = Vector3.up * Time.deltaTime * jumpForce;
            }

        }
        if (rb.velocity.y < 0)  //TODO decend faster than ascend
        {
            rb.velocity -= Vector3.up * Time.deltaTime * fallMultiplier;
            //rb.AddForce(0, -1, 0);
        }
    }
}
