﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{

    public LayerMask movementMask;
    public Rigidbody rb;

    public float movementSpeed = 20f;  //variable that detirmines the force of movement forward
    public float jumpForce = 100f;        //variable that detirmines the force of a jump
    public float fallMultiplier = 2.5f;
    public float lowJumpModifier = 2f;

    Camera cam;
    PlayerMotor motor;

	// Use this for initialization
	void Start ()
    {
        cam = Camera.main;
        motor = GetComponent<PlayerMotor>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (Input.GetMouseButton(0))  //movement by clicking
        //{
        //
        //    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;
        //
        //    if (Physics.Raycast(ray, out hit, 100, movementMask))
        //    {
        //        motor.MoveToPoint(hit.point);
        //
        //    //stop focusing any objects
        //    }
        //}
        //
        //if (Input.GetMouseButton(1))
        //{
        //
        //    Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;
        //
        //    if (Physics.Raycast(ray, out hit, 100))
        //    {
        //        //check if interactible. if it is, set as focus
        //    }
        //}
        //
        //if (Input.GetKey("w"))  //move forwards
        //{
        //    transform.position += Vector3.forward * Time.deltaTime * movementSpeed;
        //    //rb.AddForce(0, 0, forwardForce * Time.deltaTime);
        //}
        //
        //if (Input.GetKey("s"))  //move backwards
        //{
        //    transform.position -= Vector3.forward * Time.deltaTime * movementSpeed;
        //    //rb.AddForce(0, 0, -forwardForce * Time.deltaTime);
        //}
        //
        //
        //if (Input.GetKeyDown(KeyCode.Space))  //move backwards
        //{
        //    Debug.Log("Jumping. ");
        //    rb.AddForce(new Vector3(0, 100, 0), ForceMode.Impulse);
        //
        //    if (rb.velocity.y < 0)
        //    {
        //        transform.position += Vector3.up * (fallMultiplier - 1) * Physics.gravity.y * Time.deltaTime;
        //    }
        //    else if (rb.velocity.y > 0 && !Input.GetButton("space"))
        //    {
        //        transform.position += Vector3.up * (lowJumpModifier - 1) * Physics.gravity.y * Time.deltaTime;
        //    }
        //        //transform.position += Vector3.up * Time.deltaTime * jumpForce;
        //        //rb.velocity = Vector3.up * Time.deltaTime * jumpForce;
        //}
    }
}
