﻿using UnityEngine;

public class EndTrigger : MonoBehaviour {

    public gameManager gameManager;

    public playerCollision playerObject;

    private void OnTriggerEnter()
    {
        if (playerObject.GetScore() > 3)
        {

        //TODO only act if touched by player object
        
        gameManager.CompleteLevel();
        }
        else
        {
            Debug.Log("Not enough pickups. ");
        }
    }


}
